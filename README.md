# n-panes package

Sublime Text-like n-column layout shortcuts for atom editor.

Use cmd+alt+\[n\] (where n is a number) to set the layout to n adjacent vertical panes.

E.g. cmd+alt+3 will create a 3-pane layout.

https://atom.io/packages/n-panes
